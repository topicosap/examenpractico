/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reloj;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author 25774
 */
public class Tiempo extends Thread {

    private final JLabel lbl;

    public Tiempo(JLabel lbl) {
        this.lbl = lbl;
    }

    @Override
    public void run() {

        try {
            while (true) {
                Date hoy = new Date();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss");
                lbl.setText(df.format(hoy));

                Thread.sleep(1000);
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(Tiempo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
